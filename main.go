package main

import (
	"fmt"
	"net/http"
)

func resp(w http.ResponseWriter, r *http.Request) {
	URL := r.URL.String()
	method := r.Method
	fmt.Fprintf(w, "%s %s", method, URL)
}

func main() {
	http.HandleFunc("/", resp)
	http.ListenAndServe(":3000", nil)
}
